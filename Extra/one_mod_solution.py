print("Min:")
_min = int(raw_input())
print("Max:")
_max = int(raw_input())
print("Mod:")
_mod = int(raw_input())

answer = 0

for a in xrange(_min, _max + 1):
    for b in xrange(_min, _max + 1):
        solutions = 0
        for x in xrange(_mod + 1):
            if ((a * x) % _mod) == b:
                solutions += 1
        if solutions == 1:
            answer += 1
print(answer)

# Sample: 1 96 97
# Answer: 9216