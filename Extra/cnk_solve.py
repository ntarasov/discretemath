eps = 0.001


def fkt(n):
    res = 1;
    for i in xrange(n):
        res *= (i + 1)
    return res


def cnk(n, k):
    return fkt(n) / (fkt(k) * fkt(n - k))


for x in xrange(0, 20):
    if 17 * cnk(2 * x - 1, x) - 9 * cnk(2 * x, x - 1) < eps:
        print(x)
        break
