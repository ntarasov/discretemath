from itertools import permutations

sln_count = 4
cards = range(0, 6)

ans = 0
for p in permutations(cards):
    count = 0
    for i in xrange(5, -1, -1):
        if i == p[i]:
            count += 1
    if count == sln_count:
        ans += 1
print(ans)