m = [7, 11, 13]

realans = raw_input()

for x in xrange(-128, 128):
    ans = "";
    if 0 <= x < m[0] * m[1] * m[2]:
        for i in xrange(len(m)):
            ans += str(x % m[i])
    if ans == realans :
        print x
        break

# Sample: 5910
# Answer: 75