print("lower bounds (one line):")
xbounds = list(map(int, raw_input().split()))
upper_bound = 50
right_part = 30

counter = 0

for x1 in xrange(xbounds[0], upper_bound):
    for x2 in xrange(xbounds[1], upper_bound):
        for x3 in xrange(xbounds[2], upper_bound):
            for x4 in xrange(xbounds[3], upper_bound):
                if x1 + x2 + x3 + x4 == right_part:
                    counter += 1
print(counter)

# Sample: 1 -5 0 8
# Answer: 3654