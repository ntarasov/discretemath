//
// Created by Никита Тарасов on 11.06.16.
//

#ifndef DISCRETE4_POINTS_POINT_H
#define DISCRETE4_POINTS_POINT_H

#include <cstring>
#include <sstream>

struct TPoint {
    int x, y;

    TPoint(int x, int y);

    TPoint();

    bool operator==(TPoint p);

    TPoint Add(int q, int a, int b, TPoint p);

    std::string str();
};

#endif //DISCRETE4_POINTS_POINT_H
