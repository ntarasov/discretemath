//
// Created by Никита Тарасов on 11.06.16.
//

#ifndef DISCRETE4_POINTS_CALCULATIONS_H
#define DISCRETE4_POINTS_CALCULATIONS_H

#include "point.h"
#include <cmath>

class TCalc
{

private:

    int q, a, b;
    TPoint gen_point, prev_point;

    int bin_mod_pow(int num, int power);

    int get_inverse(int num);

    TPoint get_next_point_xeq();

    TPoint get_next_point_xdiff();

public:

    TCalc(int q, int a, int b, TPoint gen_point, TPoint prev_point);

    TPoint get_next_point();

    TPoint get_m();
};

#endif //DISCRETE4_POINTS_CALCULATIONS_H
