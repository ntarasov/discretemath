//
// Created by Никита Тарасов on 11.06.16.
//

#include <iostream>
#include "calculations.h"
#include <vector>

using namespace std;

int q, a, b;
TPoint gen_point;

void launch()
{
#pragma region calc
    // Дано:
    //22111
    //1
    //9 9994 4355
    //cout << (TPoint(20585, 16530)).Add(q, a, b, TPoint(6122, 22111-7519)).str() << endl;
    //TPoint g = TPoint(8676, 4765);
    //TCalc calc = TCalc(q, a, b, g, g);
    //calc.get_next_point();
    //cout << calc.get_next_point().str() << endl;
#pragma endregion
}

int main()
{
    TPoint gen = TPoint();
    cout << "Waiting for\nQ A B Px Py" << endl;
    cin >> q >> a >> b >> gen.x >> gen.y;

    launch();

    TCalc calc = TCalc(q, a, b, gen, gen);

    TPoint null_point = TPoint(),
        curr = gen;
    cout << "Generated points (first 10):" << endl;
    int count = 0;
    vector<TPoint> points;
    for (count = 0; !(curr == null_point); ++count)
    {
        points.push_back(curr);
        if (count < 10)
            cout << count + 1 << ". " << curr.str();
        curr = calc.get_next_point();
    }
    cout << "Count: " << ++count << endl;
    cout << "M: " << calc.get_m().str() << endl;
    while (true)
    {
        cout << "Waiting for [i]" << endl;
        int i;
        cin >> i;
        cout << points[i - 1].str() << endl;
    }
}