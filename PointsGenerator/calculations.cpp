//
// Created by Никита Тарасов on 12.06.16.
//

#include "point.h"
#include "calculations.h"

#define mod(num, md) (((num) % (md) + (md)) % (md))

int TCalc::bin_mod_pow(int num, int power)
{
    if (!power)
        return 1;
    if (power % 2)
        return num * bin_mod_pow(num, power - 1) % q;
    int sq = bin_mod_pow(num, power / 2) % q;
    return (sq * sq) % q;
}

int TCalc::get_inverse(int num)
{
    return bin_mod_pow(num, q - 2);
}

TPoint TCalc::get_next_point_xeq()
{
    if ((gen_point.y == prev_point.y) && prev_point.y) {
        int x = gen_point.x,
                y = gen_point.y,
                d = mod(3 * x * x, q),
                s = mod(d + a, q) * get_inverse(mod(2 * y, q));
        s %= q;

        TPoint p = TPoint();
        p.x = s * s - 2 * gen_point.x;
        p.x = mod(p.x, q);

        p.y = -gen_point.y + s * (gen_point.x - p.x);
        p.y = mod(p.y, q);
        return p;
    }
    else
        return TPoint();
}

TPoint TCalc::get_next_point_xdiff()
{
    int num = mod(prev_point.y - gen_point.y, q),
            denum = get_inverse(mod(prev_point.x - gen_point.x, q)),
            s = mod(num * denum, q);

    TPoint p = TPoint();
    p.x = mod(s * s - prev_point.x - gen_point.x, q);
    p.y = mod(-gen_point.y + s * (gen_point.x - p.x), q);
    return p;
}

TCalc::TCalc(int q, int a, int b, TPoint gen_point, TPoint prev_point)
{
    this->q = q;
    this->a = a;
    this->b = b;
    this->gen_point = gen_point;
    if (prev_point == TPoint())
        prev_point = gen_point;
    this->prev_point = prev_point;
}

TPoint TCalc::get_next_point()
{
    if (gen_point.x == prev_point.x)
        prev_point = get_next_point_xeq();
    else
        prev_point = get_next_point_xdiff();
    return prev_point;
}

TPoint TCalc::get_m()
{
    for (int x = 2; x < q; ++x) {
        double y = sqrt(mod((x * x * x) % q + x % q - 9, q));
        if (y == (int) y)
            return TPoint(x, (int) y);
    }
    return TPoint();
}
