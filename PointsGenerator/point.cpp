//
// Created by Никита Тарасов on 12.06.16.
//

#include "point.h"
#include "calculations.h"

TPoint::TPoint(int x, int y)
{
    this->x = x;
    this->y = y;
}

TPoint::TPoint()
{
    x = 0;
    y = 0;
}

bool TPoint::operator==(TPoint p)
{
    return p.x == x && p.y == y;
}

TPoint TPoint::Add(int q, int a, int b, TPoint p)
{
    TCalc calc = TCalc(q, a, b, *this, p);
    return calc.get_next_point();
}

std::string TPoint::str()
{
    std::ostringstream oss;
    oss << '(' << x << "; " << y << ')' << std::endl;
    return oss.str();
}