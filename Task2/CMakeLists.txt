cmake_minimum_required(VERSION 3.5)
project(se_solution)

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11")

set(SOURCE_FILES main.cpp card.h test.h)
add_executable(se_solution ${SOURCE_FILES})