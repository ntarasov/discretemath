//
// Created by Никита Тарасов on 13.06.16.
//

#ifndef SE_SOLUTION_CARD_H
#define SE_SOLUTION_CARD_H

struct Card {

	int value, suit;

private:
	int v_to_cnt(int val)
	{
		switch (val)
		{
			case 'A': return 0;
			case 'T': return 10;
			case 'J': return 11;
			case 'Q': return 12;
			case 'K': return 13;
			default: return val - '0';
		}
	}

	int s_to_cnt(int val)
	{
		switch (val)
		{
			case 'S': return 0;
			case 'C': return 1;
			case 'D': return 2;
			case 'H': return 3;
			default: return 4;
		}
	}

public:

	Card()
	{
	}

	Card(int suit, int value)
	{
		this->value = v_to_cnt(value);
		this->suit = s_to_cnt(suit);
	}

	Card(const char* str)
	{
		this->value = v_to_cnt(str[1]);
		this->suit = s_to_cnt(str[0]);
	}

	bool operator==(Card another_card)
	{
		return value == another_card.value &&
			   suit == another_card.suit;
	}

	bool operator<(const Card& card) const
	{
		return value < card.value;
	}

	std::string str()
	{
		return std::string() + (char)suit + (char)value;
	}
};

#endif //SE_SOLUTION_CARD_H
