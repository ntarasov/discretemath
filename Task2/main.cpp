#include <iostream>
#include <string>
#include <ctime>
#include "card.h"

using namespace std;

#define DECK_SIZE (52 - 3 - 5)
#define IGNORED_SIZE 5
#define HAND_START_SIZE 3
#define HAND_SIZE 7

string all_values = "A23456789TJQK",
		all_suits = "SCDH";

Card ignored_cards[] = { Card("H6"), Card("HA"), Card("S6"),
						 Card("SQ"), Card("D2") },
		hand_cards[8] = { Card("SK"), Card("D4"), Card("DA") },
		deck[DECK_SIZE];

bool is_unused(Card card)
{
	for (int i = 0; i < IGNORED_SIZE; ++i)
		if (ignored_cards[i] == card)
			return true;
	for (int i = 0; i < HAND_START_SIZE; ++i)
		if (hand_cards[i] == card)
			return true;
	return false;
}

void make_deck()
{
	int last = 0;
	for (auto val : all_values)
		for (auto suit : all_suits)
		{
			Card c = Card(suit, val);
			if (!is_unused(c))
				deck[last++] = c;
		}
}

#include "test.h"
#define is_solution() (test_2k() && !test_4k() && !test_3p2() && !test_sf())

int main()
{
	clock_t start = clock();
	make_deck();

	int solution_counter = 0, total = 0;
	for (int first = 0; first < DECK_SIZE; ++first)
		for (int second = first + 1; second < DECK_SIZE; ++second)
			for (int third = second + 1; third < DECK_SIZE; ++third)
				for (int fourth = third + 1; fourth < DECK_SIZE; ++fourth)
				{
					hand_cards[HAND_START_SIZE] = deck[first];
					hand_cards[HAND_START_SIZE + 1] = deck[second];
					hand_cards[HAND_START_SIZE + 2] = deck[third];
					hand_cards[HAND_START_SIZE + 3] = deck[fourth];
					if (is_solution())
						++solution_counter;
					++total;
				}
	cout.precision(2);
	cout << double(clock() - start) / CLOCKS_PER_SEC << "s. elapsed" << endl;
	cout << "Solutions: " << solution_counter << endl;
	cout << "Total: " << total << endl;
	cout << "Percentage: " << (double(solution_counter) / total);
	return 0;
}