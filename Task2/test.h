//
// Created by Никита Тарасов on 13.06.16.
//

#ifndef SE_SOLUTION_TEST_H
#define SE_SOLUTION_TEST_H

#include <algorithm>

int test_sf()
{
	int counters[4] = {};

	int found = 0 , suit = 0;
	for (int i = 0; i < HAND_SIZE; ++i)
	{
		if (++counters[hand_cards[i].suit] == 5)
		{
			found = 1;
			suit = hand_cards[i].suit;
		}
	}
	if (!found) return 0;
	Card hand_copy[8];
	int hand_length = 0;
	for (int i = 0; i < 7; ++i)
		if (hand_cards[i].suit == suit)
			hand_copy[hand_length++] = hand_cards[i];
	sort(hand_copy, hand_copy + hand_length);
	for (int i = 1; i < hand_length; ++i)
		if (hand_copy[i].value - hand_copy[i - 1].value > 1)
			return 0;
	return 1;
}

int test_4k()
{
	int counters[16] = {};
	for (int i = 0; i < HAND_SIZE; ++i)
		if (++counters[hand_cards[i].value] == 4)
			return 1;
	return 0;
}

int test_3p2()
{
	int counters[16] = {};
	for (int i = 0; i < HAND_SIZE; ++i)
		++counters[hand_cards[i].value];
	int found_3 = 0, found_2 = 0;
	for (int i = 0; i < HAND_SIZE; ++i)
	{
		if (counters[i] == 3)
			found_3 = 1;
		if (counters[i] == 2)
			found_2 = 1;
	}
	return found_3 && found_2;
}

int test_2k()
{
	int counters[16] = {};
	for (int i = 0; i < HAND_SIZE; ++i)
		if (++counters[hand_cards[i].value] == 2)
			return 1;
	return 0;
}

#endif //SE_SOLUTION_TEST_H
